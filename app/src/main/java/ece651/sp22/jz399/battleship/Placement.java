package ece651.sp22.jz399.battleship;

public class Placement {
    private final char Orientation;
    private final Coordinate where;

    public Placement(String input) throws IllegalArgumentException {
        input = input.toUpperCase();
        if (input.length() != 3)
            throw new IllegalArgumentException("wrong input format\n");

        String coordinate_str = input.substring(0, 2);
        this.where = new Coordinate(coordinate_str);

        if (input.charAt(2) == 'H' || input.charAt(2) == 'V')
            this.Orientation = input.charAt(2);
        else
            throw new IllegalArgumentException("invalid Orientation\n");

    }

    public Placement(Coordinate where, char orientation) throws IllegalArgumentException {
        orientation = Character.toUpperCase(orientation);
        if (orientation == 'H' || orientation == 'V')
            this.Orientation = orientation;
        else
            throw new IllegalArgumentException("invalid Orientation\n");
        this.where = where;
    }

    public char getOrientation() {
        return Orientation;
    }

    public Coordinate getCoordinate() {
        return where;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Orientation;
        result = prime * result + ((where == null) ? 0 : where.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass().equals(getClass())){
            Placement c = (Placement) obj;
            return c.where.equals(where) && c.Orientation == Orientation;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Placement [Orientation=" + Orientation + ", where=" + where + "]";
    }

}
