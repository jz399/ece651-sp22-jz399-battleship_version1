package ece651.sp22.jz399.battleship;

import java.util.ArrayList;
//import ece651.sp22.jz399.battleship.NoCollisionRuleChecker;
import java.util.HashSet;


public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    private final ArrayList<Ship<T>>myShips;  
    private final PlacementRuleChecker<T> placementChecker;
    private HashSet<Coordinate> enemyMisses;
    private final T missInfo;

    /*
     * Constructs a BattleShipBoard with the specified width
     * and height
     * 
     * @param w is the width of the newly constructed board.
     * 
     * @param h is the height of the newly constructed board.
     * 
     * @throws IllegalArgumentException if the width or height are less than or
     * equal to zero.
     */
    BattleShipBoard(int width, int height, T missInfo) {
        if (width <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + height);
        }
        this.height = height;
        this.width = width;
        this.myShips = new ArrayList<Ship<T>>();
        this.placementChecker = new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null));  //后期继续添加规则
        this.enemyMisses = new HashSet<Coordinate>();
        this.missInfo = missInfo;
    }

    public int getWidth() {
        return width; 
    }

    public int getHeight() {
        return height;
    }

    public ArrayList<Ship<T>> getMyShips(){
        return myShips;
    }

    public String tryAddShip(Ship<T> toAdd){
        String situtaion = placementChecker.checkPlacement(toAdd, this);
        if(situtaion == null){  //add ship successfully
            myShips.add(toAdd);
            return null;
        }
        else  // fail to add ship 
            return situtaion;
            
    }
 
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where,true);
    }

    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    //return what is at in two perspective
    protected T whatIsAt(Coordinate where, boolean isSelf){
        // check whether ship occupies this coordinate
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
              return s.getDisplayInfoAt(where, isSelf);
            }
        }

        // check whether miss(only check in enemy perspective)
        if(isSelf == false && enemyMisses.contains(where) == true)
            return missInfo;

        return null;  
    }

    public Ship<T> fireAt(Coordinate c){
        //search for a ship that occupies coordinate c
        for(Ship<T> ship:myShips){
            if(ship.occupiesCoordinates(c) == true){  
                ship.recordHitAt(c);
                return ship;
            }
        }

        //nothing get hit, record the miss
        enemyMisses.add(c);  
        return null;
    }


}