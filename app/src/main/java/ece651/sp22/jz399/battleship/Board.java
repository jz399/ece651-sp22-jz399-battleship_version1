package ece651.sp22.jz399.battleship;

import java.util.ArrayList;

public interface Board<T> {
    /**
     * return the Width of the board
     * 
     * @return the Width of the board
     */
    public int getWidth();

    /**
     * return the Height of the board
     * 
     * @return the Height of the board
     */
    public int getHeight();

    /**
     * return all the Ships on the board
     * 
     * @return the Ships on the board
     */
    public ArrayList<Ship<T>> getMyShips();

    /**
     * return what is at Coordinate where in the Board in self perspective.
     * 
     * @param where the Coordinate we need to search
     * @return what the board will display in this coordinate
     */
    public T whatIsAtForSelf(Coordinate where);

    /**
     * return what is at Coordinate where in the Board in enemy perspective.
     * 
     * @param where the Coordinate we need to search
     * @return what the board will display in this coordinate
     */
    public T whatIsAtForEnemy(Coordinate where);

    /**
     * try to add a ship to the board
     * 
     * @param toAdd the ship we need to add
     * @return null if add successfully,otherwise return the string points out
     *         failure reasons.
     */
    public String tryAddShip(Ship<T> toAdd);

    /**
     * fire at the Coordinate. required to pass valid Coordinate. Function allows
     * to fire at the same coordinate many times.
     * @param c the Coordinate we want to fire at.
     * @return null if we miss,otherwise return the ship we fire at.
     */
    public Ship<T> fireAt(Coordinate c);

}