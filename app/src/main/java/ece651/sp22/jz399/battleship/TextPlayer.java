package ece651.sp22.jz399.battleship;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final String name;
    final ArrayList<String> shipsToPlace; // store a list of ship names
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns; // a map from ship name to lambda
                                                                                 // function

    // constructor
    public TextPlayer(Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
            AbstractShipFactory<Character> shipFactory, String name) {
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.name = name;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        setupShipCreationList();
        setupShipCreationMap();
    }

    private void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    }

    private void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(1, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(1, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(1, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(1, "Carrier"));
    }

    // 根据输入坐标，返回一个placement
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null)
            throw new EOFException("does not enter placement, cause EOF excepetion.\n");
        return new Placement(s);
    }

    // 根据输入的坐标，生成BasicShip并加入地图中，输出地图
    // Function<Placement, Ship<Character>> is an object an apply method that takes
    // a Placement and returns a Ship<Character>
    private boolean doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        // handle exceptions caused by wrong format Placement
        Placement p;
        try {
            p = readPlacement("Player " + name + " where do you want to place a" + shipName + "?");
        } catch (IllegalArgumentException e) {
            out.println(e.getMessage());
            return false;
        }
        Ship<Character> s = createFn.apply(p);
        String situtaion = theBoard.tryAddShip(s);
        if (situtaion == null) { // add ship successfully
            out.print(view.displayMyOwnBoard());
            out.print("\n");
            return true;
        } else { // fail to add ship
            out.print(situtaion);
            return false;
        }
    }

    // 完成船只放置阶段
    public void doPlacementPhase() throws IOException {
        // show an empty board
        out.println(view.displayMyOwnBoard());

        // show instructions
        String instruction = "--------------------------------------------------------------------------------\n" +
                "Player " + name + " : you are going to place the following ships (which are allrectangular).\n" +
                "For each ship, type the coordinate of the upper left side of the ship, followed by either H (for horizontal) or V (for vertical).\n"
                +
                "For example M4H would place a ship horizontally starting at M4 and going to the right.\n" +
                "You have:\n 2 Submarines ships that are 1x2 \n 3 Destroyers that are 1x3\n 3 Battleships that are 1x4\n 2 Carriers that are 1x6.\n"
                +
                "--------------------------------------------------------------------------------";
        out.println(instruction);

        // place ships
        for (int i = 0; i < shipsToPlace.size(); i++) {
            String shipName = shipsToPlace.get(i);
            if (doOnePlacement(shipName, shipCreationFns.get(shipName)) == false) // 假如船加入失败，重新加入该船
                i--;
        }
    }

    // 当前player发起一次攻击
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException {
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        String myHeader = "Your ocean";
        String enemyHeader = "Player " + enemyName + "'s ocean:\n";

        // diplay the board
        out.println("Player " + name + "'s turn:\n");
        out.println(view.displayMyBoardWithEnemyNextToIt(enemyView, myHeader, enemyHeader));

        // prompt to fire
        Coordinate c;
        while (true) {
            out.print("Enter a coordinate you want fire at:\n");
            String s = inputReader.readLine();
            if (s == null)
                throw new EOFException("does not enter coordinate, cause EOF excepetion.\n");
            try {
                c = new Coordinate(s); // 这里只保证坐标的格式是正确的
                if (c.getRow() >= enemyBoard.getHeight() || c.getColumn() >= enemyBoard.getWidth()) {  //保证坐标在地图内
                    throw new IllegalArgumentException("coordinate is out of the board.\n");
                }
                break;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }
        Ship<Character> hitShip = enemyBoard.fireAt(c);  //这里不检查是否重复击中相同目标，原则上允许。

        // report result
        if (hitShip == null)
            out.println("You missed!");
        else {
            out.println("You hit a " + hitShip.getName() + "!");
        }

    }

}