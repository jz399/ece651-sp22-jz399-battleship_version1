package ece651.sp22.jz399.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
    protected HashMap<Coordinate, Boolean> myPieces; // record every coordinate in ship, True->hit, False->unhit. Not
                                                     // exist-> not in this ship.
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) { // 传入的对象是可迭代的，实现了iterable接口
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        this.myPieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c : where) {
            myPieces.put(c, false);
        }
    }

    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);

    }

    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) throws IllegalArgumentException {
        checkCoordinateInThisShip(where);
        if(myShip == true)
            return myDisplayInfo.getInfo(null, myPieces.get(where));
        else
            return enemyDisplayInfo.getInfo(null, myPieces.get(where));
    }

    @Override
    public boolean isSunk() throws IllegalArgumentException {
        for (boolean value : myPieces.values()) {
            if (value == false)
                return false;
        }
        return true;
    }

    @Override
    public void recordHitAt(Coordinate where) throws IllegalArgumentException {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    @Override
    public boolean wasHitAt(Coordinate where) throws IllegalArgumentException {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    protected void checkCoordinateInThisShip(Coordinate c) throws IllegalArgumentException {
        if (myPieces.containsKey(c) == false)
            throw new IllegalArgumentException("Input Coordinate does not exist in Hashset\n");
    }

    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }
}