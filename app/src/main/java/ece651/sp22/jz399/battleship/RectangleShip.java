package ece651.sp22.jz399.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
    private final String name; 

    // generate coordinates for rect ship. Computing in this class and then passing
    // HashMap to parent class is to make parent class more generic to different
    // type of ship.
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height, Character orientation) {
        HashSet<Coordinate> set = new HashSet<Coordinate>();

        int row_upperLeft = upperLeft.getRow();
        int col_upperLeft = upperLeft.getColumn();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (orientation == 'V') // vertical
                    set.add(new Coordinate(row_upperLeft + i, col_upperLeft + j));
                else // horizontal
                    set.add(new Coordinate(row_upperLeft + j, col_upperLeft + i));
            }
        }
        return set;
    }

    public RectangleShip(String name, Coordinate upperLeft, Character orientation, int width, int height,
            ShipDisplayInfo<T> myShipDisplayInfo,
            ShipDisplayInfo<T> EnemyShipDisplayInfo) {
        super(makeCoords(upperLeft, width, height, orientation), myShipDisplayInfo, EnemyShipDisplayInfo);
        this.name = name;
    }

    public RectangleShip(String name, Coordinate upperLeft, Character orientation, int width, int height, T data,
            T onHit) {
        this(name, upperLeft, orientation, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
    }

    public RectangleShip(Coordinate upperLeft, Character orientation, T data, T onHit) { // Basic Ship
        this("testship", upperLeft, orientation, 1, 1, data, onHit);
    }

    @Override
    public String getName() {
        return name;
    }
}