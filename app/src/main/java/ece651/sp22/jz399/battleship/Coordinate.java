package ece651.sp22.jz399.battleship;

public class Coordinate {
    private final int row;
    private final int col;

    public Coordinate(int r, int c) {
        this.row = r;
        this.col = c;
    }

    public Coordinate(String descr) {
        descr = descr.toUpperCase();
        if (descr.length() != 2)
            throw new IllegalArgumentException("Coordinate is not correct\n");

        if (descr.charAt(0) >= 'A' && descr.charAt(0) <= 'Z')
            this.row = descr.charAt(0) - 'A';
        else
            throw new IllegalArgumentException("Invalid row number\n");

        if (descr.charAt(1) >= '0' && descr.charAt(1) <= '9')
            this.col = descr.charAt(1) - '0';
        else
            throw new IllegalArgumentException("Invalid col number\n");

    }

    public int getRow(){
        return row;
    }

    public int getColumn(){
        return col;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + row;
        return result;
    }

    @Override
    public String toString() {
        return "Coordinate [col=" + col + ", row=" + row + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass()) // must be the same class
            return false;

        Coordinate other = (Coordinate) obj;   //即使是用一个类别，也最好显示casting.Tell the compiler how to execute.
        if (col != other.col || row != other.row)
            return false;

        return true;
    }

}