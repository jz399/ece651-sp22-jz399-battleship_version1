package ece651.sp22.jz399.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

public class BattleShipBoardTest {
    @Test
    void testGetHeight() {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        assertEquals(20, b1.getHeight());

    }

    @Test
    void testGetWidth() {
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
        assertEquals(10, b2.getWidth());
    }

    @Test
    void testInvalidArg() {
        // 第一个参数是期望抛出的异常. 第二个参数是lambda表达式（匿名函数）
        // ()代表函数要传入的参数，-> 为语法要求， 箭头后面的是函数体。
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, ArrayList<Ship<T>> addShipes, T[][] expected) {
        // check initial empey board
        for (int i = 0; i < b.getHeight(); i++) {
            for (int j = 0; j < b.getWidth(); j++) {
                assertEquals(b.whatIsAtForSelf(new Coordinate(i, j)), null);
            }
        }

        // add ships
        for (Ship<T> s : addShipes) {
            b.tryAddShip(s);
        }

        // check
        for (int i = 0; i < b.getHeight(); i++) {
            for (int j = 0; j < b.getWidth(); j++) {
                assertEquals(expected[i][j], b.whatIsAtForSelf(new Coordinate(i, j)));
            }
        }

    }

    @Test
    void testTryAddShip() {
        BattleShipBoard<Character> b = new BattleShipBoard<>(3, 3, 'X');
        ArrayList<Ship<Character>> ships = new ArrayList<Ship<Character>>();
        ships.add(new RectangleShip<Character>(new Coordinate(0, 0), 'V', 's', '*')); // generate testship
        ships.add(new RectangleShip<Character>(new Coordinate(1, 1), 'V', 's', '*'));
        ships.add(new RectangleShip<Character>(new Coordinate(2, 2), 'V', 's', '*'));

        Character[][] expected = { { 's', null, null }, { null, 's', null }, { null, null, 's' } };

        checkWhatIsAtBoard(b, ships, expected);

    }

    @Test
    void testFireAt() {
        BattleShipBoard<Character> b = new BattleShipBoard<>(5, 5, 'X');
        V1ShipFactory f = new V1ShipFactory();
        Ship<Character> ship = f.makeDestroyer(new Placement("a0v"));
        b.tryAddShip(ship);
        assertSame(ship, b.fireAt(new Coordinate(0, 0)));
        assertSame(ship, b.fireAt(new Coordinate(1, 0)));
        assertFalse(ship.isSunk());
        assertSame(ship, b.fireAt(new Coordinate(2, 0)));
        assertSame(null, b.fireAt(new Coordinate(0, 1)));
        assertTrue(ship.isSunk());
    }
}
