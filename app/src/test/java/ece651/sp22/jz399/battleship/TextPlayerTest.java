package ece651.sp22.jz399.battleship;

import org.junit.jupiter.api.Test;
import java.io.*;
import static org.junit.jupiter.api.Assertions.*;

public class TextPlayerTest {

    // helper function
    private TextPlayer createTextPlayer(int w, int h, String inputData, PrintStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer(board, input, output, shipFactory, "A");
    }

    @Test
    void testReadPlacement() throws IOException {
        String input = "B2V\nC8H\na4v\n"; // 输入
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, input, new PrintStream(bytes, true));

        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); // did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); // should have printed prompt and newline
            bytes.reset(); // clear out bytes for next time around
        }
    }

    @Test
    void testdoPlacements() throws IOException {
        //String input = "B2V\nC8H\na4v\nb7h\nI7h\nd9h\nq5h\nM7h\ne1h\ng4h\n"; // 输入
        String input = "a0h\nb0h\nc0h\nd0h\ne0h\nf0h\ng0h\nh0h\ni0h\nj0h\nk0h\nl0h\n";
        TextPlayer player = createTextPlayer(10, 20, input, System.out);

        player.doPlacementPhase();
    }

}
